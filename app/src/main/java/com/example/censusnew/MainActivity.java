package com.example.censusnew;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button addData, view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addData= findViewById(R.id.addData);
        view= findViewById(R.id.viewData);

        addData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNewActivitySave();
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openNewActivityView();
            }
        });
    }
    public void openNewActivitySave(){
        Intent intent = new Intent(MainActivity.this, AddData.class);
        startActivity(intent);
    }

    public void openNewActivityView(){
        Intent intent = new Intent(MainActivity.this, ViewData.class);
        startActivity(intent);
    }


}