package com.example.censusnew;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;

public class AddData extends AppCompatActivity {

    EditText name, age;
    RadioButton male, female;
    RadioGroup gender;
    Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data);

         name = findViewById(R.id.name);
         age = findViewById(R.id.age);
         gender = findViewById(R.id.radioGroup2);
         save = findViewById(R.id.save);


         save.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent intent= new Intent(AddData.this, MainActivity.class);
                 startActivity(intent);
             }
         });






    }
}