package com.example.censusnew;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import java.util.Objects;

public class SplashScreen extends AppCompatActivity {

    private boolean flag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Objects.requireNonNull(getSupportActionBar()).hide();

        SharedPreferences sharedPreferences = getSharedPreferences("systemUser",MODE_PRIVATE);
        flag= sharedPreferences.getBoolean("isRegistered",false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //if(password!= isnull())- Splash.this,Login.class
                Intent intent;

                if(flag)
                    intent= new Intent(SplashScreen.this,Registration.class);

                else{
                    intent= new Intent(SplashScreen.this,Login.class);
                }
                startActivity(intent);
                finish();
            }
        },3000);
    }

}