package com.example.censusnew;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registration extends AppCompatActivity {

    EditText password;
    Button register;
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        password= findViewById(R.id.password);
        register= findViewById(R.id.register);

        preferences= getSharedPreferences("systemUser",0);//0- private
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String passwordValue= password.getText().toString();
                SharedPreferences.Editor editor= preferences.edit();
                editor.putString("password",passwordValue);
                editor.apply();
                Toast.makeText(Registration.this, "User registered", Toast.LENGTH_SHORT).show();
            }
        });

        register.setOnClickListener((view -> {
            Intent intent = new Intent(Registration.this, Login.class);
            startActivity(intent);
            finish();
        }));
    }

}