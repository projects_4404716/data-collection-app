package com.example.censusnew;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    EditText password;
    Button login;
    private int count=3;
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        password= findViewById(R.id.password);
        login= findViewById(R.id.login);

        preferences= getSharedPreferences("systemUser",0);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String passwordValue= password.getText().toString();

                String registeredPasswored= preferences.getString("password","");

                if (passwordValue.equals(registeredPasswored)){
                    Intent intent= new Intent(Login.this,MainActivity.class);
                    startActivity(intent);
                }
                else {
                    count--;
                    if(count==2||count==1)
                        Toast.makeText(Login.this, count+" More attempts left.", Toast.LENGTH_SHORT).show();
                    else {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }
                }
            }
        });
    }

}